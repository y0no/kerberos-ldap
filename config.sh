#!/bin/bash

[[ "TRACE" ]] && set -x

: ${REALM:=AMAZON}
: ${DOMAIN_REALM:=amazon}
: ${KERB_MASTER_KEY:=masterkey}
: ${KERB_ADMIN_USER:=admin}
: ${KERB_ADMIN_PASS:=admin}
: ${SEARCH_DOMAINS:=krb.amazon}
: ${LDAP_DC:=example,dc=com}
: ${LDAP_USER:=admin}
: ${LDAP_PASS:=admin}

fix_nameserver() {
  cat>/etc/resolv.conf<<EOF
nameserver $NAMESERVER_IP
search $SEARCH_DOMAINS
EOF
}

fix_hostname() {
  sed -i "/^hosts:/ s/ *files dns/ dns files/" /etc/nsswitch.conf
}

create_config() {
  : ${KDC_ADDRESS:=$(hostname -f)}

  cat>/etc/krb5.conf<<EOF
[logging]
 default = FILE:/var/log/kerberos/krb5libs.log
 kdc = FILE:/var/log/kerberos/krb5kdc.log
 admin_server = FILE:/var/log/kerberos/kadmind.log

[libdefaults]
 default_realm = $REALM
 dns_lookup_realm = false
 dns_lookup_kdc = false
 ticket_lifetime = 24h
 renew_lifetime = 7d
 forwardable = true

[realms]
 $REALM = {
  kdc = $KDC_ADDRESS
  admin_server = $KDC_ADDRESS
  default_domain = $DOMAIN_REALM
  database_module = openldap_ldapconf
 }

[domain_realm]
 .$DOMAIN_REALM = $REALM
 $DOMAIN_REALM = $REALM

[dbdefaults]
  ldap_kerberos_container_dn = dc=$LDAP_DC

[dbmodules]
  openldap_ldapconf = {
          db_library = kldap
          ldap_kdc_dn = "cn=$LDAP_USER,dc=$LDAP_DC"

          # this object needs to have read rights on
          # the realm container, principal container and realm sub-trees
          ldap_kadmind_dn = "cn=$LDAP_USER,dc=$LDAP_DC"

          # this object needs to have read and write rights on
          # the realm container, principal container and realm sub-trees
          ldap_service_password_file = /var/kerberos/krb5kdc/service.keyfile
          ldap_servers = ldap://ldap
          ldap_conns_per_server = 5
  }
EOF
}

create_db() {
  /usr/sbin/kdb5_util -P $KERB_MASTER_KEY -r $REALM create -s
}

init_ldap() {
  /usr/sbin/kdb5_ldap_util -D cn=$LDAP_USER,dc=$LDAP_DC create -subtrees dc=$LDAP_DC -r $REALM -s -H ldap://ldap <<EOF
$LDAP_PASS
$KERB_ADMIN_PASS
$KERB_ADMIN_PASS
EOF

  /usr/sbin/kdb5_ldap_util -D cn=$LDAP_USER.,dc=$LDAP_DC stashsrvpw -f /var/kerberos/krb5kdc/service.keyfile cn=$LDAP_USER,dc=$LDAP_DC <<EOF
$LDAP_PASS
$LDAP_PASS
$LDAP_PASS
EOF
}

start_kdc() {
  /etc/rc.d/init.d/krb5kdc start
  /etc/rc.d/init.d/kadmin start

  chkconfig krb5kdc on
  chkconfig kadmin on
}

restart_kdc() {
  /etc/rc.d/init.d/krb5kdc restart
  /etc/rc.d/init.d/kadmin restart
}

create_admin_user() {
  kadmin.local -q "addprinc -pw $KERB_ADMIN_PASS $KERB_ADMIN_USER/admin"
  echo "*/admin@$REALM *" > /var/kerberos/krb5kdc/kadm5.acl
}

main() {
  fix_nameserver
  fix_hostname
  create_config

  if [ ! -f /kerberos_initialized ]; then
    create_db
    create_admin_user
    mkdir /var/log/kerberos
    init_ldap
    start_kdc

    touch /kerberos_initialized
  fi

  if [ ! -f /var/kerberos/krb5kdc/principal ]; then
    while true; do sleep 1000; done
  else
    start_kdc
    tailf /dev/null
  fi
}

[[ "$0" == "$BASH_SOURCE" ]] && main "$@"
